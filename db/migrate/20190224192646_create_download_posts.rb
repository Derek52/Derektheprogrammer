class CreateDownloadPosts < ActiveRecord::Migration[6.0]
  def change
    create_table :download_posts do |t|
      t.integer :download_id
      t.integer :post_id
      t.timestamps
    end
  end
end
