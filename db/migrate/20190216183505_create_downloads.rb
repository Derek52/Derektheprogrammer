class CreateDownloads < ActiveRecord::Migration[6.0]
  def change
    create_table :downloads do |t|
      t.string :file
      t.string :description

      t.timestamps
    end
  end
end
