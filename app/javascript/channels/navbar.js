document.addEventListener('DOMContentLoaded', () => {
	const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

	if ($navbarBurgers.length > 0) {
		$navbarBurgers.forEach( burgerItem => {

			burgerItem.addEventListener('click', () => {
				const target = burgerItem.dataset.target;
				const $target = document.getElementById(target);
				burgerItem.classList.toggle('is-active');
				$target.classList.toggle('is-active');
			});
		});
	}
});
