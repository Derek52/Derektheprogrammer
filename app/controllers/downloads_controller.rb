class DownloadsController < ApplicationController

  def index
    @downloads = Download.all
  end

  def show
    @download = Download.find(params[:id])
  end

  def new
    @download = Download.new
  end

  def create
    @download = Download.create(download_params)
  end

  def edit
    @download = Download.find(params[:id])
  end

  def update
    @download = Download.find(params[:id])
    @download.update(download_params)
    redirect_to @download
  end

  private
  def download_params
    params.require(:download).permit(:file, :description, post_ids:[])
  end
end
