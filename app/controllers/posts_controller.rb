class PostsController < ApplicationController
  #include Pagy::Backend

  def index
    #@pagy, @posts = pagy(Post.order(:created_at => :asc))
    #<%== pagy_nav(@pagy) %>, add this to post index page, when pagination becomes necessary
    @posts = Post.published
  end

  def show
    @post = Post.find_by(slug: params[:id])
  end

  def edit
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post
    end
  end

  def update
    if @post.update(post_params)
      redirect_to @post
    end
  end

  def destroy
    @post = Post.find_by(slug: params[:id])
    @post.destroy
    redirect_to :index
  end

  private
  def post_params
    params.require(:post).permit(:title, :body, :thumbnail, :tags, :category, :published, :slug, download_ids:[])
  end
end
