class Admin::PostsController < ApplicationController
  ADMIN = { Rails.application.credentials.ADMIN_USERNAME => Rails.application.credentials.ADMIN_PASSWORD }
  #ADMIN = { "derek" => "frank" }
  before_action :authenticate
  
  def index
    @posts = Post.all
    #@pagy, @posts = pagy(Post.order(:created_at => :asc))
  end

  def show
    @post = Post.find_by(slug: params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post, notice: "Post created"
    else
      render action: "new"
    end
  end

  def edit
    @post = Post.find_by(slug: params[:id])
  end

  def update
    @post = Post.find_by(slug: params[:id])
    @post.update(post_params)
    redirect_to @post
  end

  def destroy
    @post = Post.find_by(slug: params[:id])
    @post.destroy
    flash[:notice] = "Post deleted"
  end

  private
  def authenticate
    authenticate_or_request_with_http_digest do |username|
      ADMIN[username]
    end
  end

  def post_params
    params.require(:post).permit(:title, :body, :thumbnail, :tags, :category, :published, :slug, download_ids:[])
  end
end
