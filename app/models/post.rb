class Post < ApplicationRecord
  has_many :download_posts
  has_many :downloads, through: :download_posts
  has_rich_text :body

  scope :published, lambda { where(:published => true) }

  before_validation :sanitize, :slugify

  def slugify
    self.slug = self.title.downcase.gsub(" ", "-").gsub('.', '-')
  end

  def sanitize
    self.title = self.title.strip
  end

  def publishedTime
    self.created_at.to_time.strftime('Published: %B %e,&nbsp; %Y').html_safe
  end

  def truncatePost
    if body
      self.body.to_s.truncate(350, omission: '... (click for more)').html_safe
    end
  end

  def to_param
    self.slug
  end
end
