class Download < ApplicationRecord
  has_many :download_posts
  has_many :posts, through: :download_posts
end
