# Derektheprogrammer

This is a blog website built in Ruby on Rails. I'm currently updating it to the Rails 6.0 beta as an experiment. I'm liking Rails 6 so far.

I'm using Ruby 2.6.0-dev, that I installed with Rbenv on Arch Linux. Rails recognizes this as 2.7.0. I've also had this running with 2.5.1.

Just change the ruby version in your Gemfile and delete the .ruby-version file.

It uses postgresql as the database backend.
