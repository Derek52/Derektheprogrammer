Rails.application.routes.draw do
  root 'posts#index'

  #add admin route to credentials file

  get '/about' => 'static_pages#about'

  #for a little added security, i've hidden my admin login page, using this variable set in my credentials.yml.enc file
  scope Rails.application.credentials.dig(:admin_url) do 
    namespace :admin do
      resources :posts
      resources :downloads
    end
  end

  resources :posts, only: [:index, :show] do
    resources :downloads, only: [:index, :show]
  end
  resources :downloads, only: [:index, :show]
end
